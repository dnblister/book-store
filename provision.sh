#!/usr/bin/env bash

echo 'LC_ALL="en_US.UTF-8"' >> /etc/default/locale

echo 'deb http://apt.postgresql.org/pub/repos/apt/ xenial-pgdg main' >> /etc/apt/sources.list.d/pgdg.list
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add -

add-apt-repository -y ppa:ondrej/php

apt-get update

apt-get -y install nginx

apt-get -y install postgresql-10

apt-get -y install php7.2-fpm php7.2-common php7.2-pgsql php7.2-xml php7.2-intl

ln -s /opt/project/book-store.conf /etc/nginx/sites-enabled/book-store.conf

rm /etc/nginx/sites-enabled/default

service nginx restart

echo "# Application 'book-storage'
host	storage		librarian	127.0.0.1/32		password" >> /etc/postgresql/10/main/pg_hba.conf

sudo -u postgres psql -c "CREATE USER librarian PASSWORD 'librarian';"
sudo -u postgres createdb storage -O "librarian"

echo "localhost:5432:storage:librarian:librarian" >> /home/vagrant/.pgpass
chmod 0600 /home/vagrant/.pgpass
chown vagrant:vagrant /home/vagrant/.pgpass

sudo -u vagrant psql -U librarian -h localhost -d storage < /opt/project/src/etc/schema.sql

sudo -u vagrant mkdir /opt/project/web/images