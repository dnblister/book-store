<?php
/**
 *
 * @author  Zarubin Kirill <dnblister@gmail.com>
 *
 * @version 1.0
 */

namespace Tests\AppBundle\Exceptions\Entity;


use Exception;

class AuthorExistException extends Exception
{
    protected $message = 'Нельзя добавить уже существующего автора.';
}