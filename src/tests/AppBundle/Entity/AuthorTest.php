<?php
/**
 *
 * @author  Zarubin Kirill <dnblister@gmail.com>
 *
 * @version 1.0
 */

namespace Tests\AppBundle\Entity;

use AppBundle\DTO\AuthorData;
use AppBundle\Entity\Author;
use PHPUnit\Framework\TestCase;

class AuthorTest extends TestCase
{
    /**
     * @var Author
     */
    private $author;

    public function setUp()
    {
        $authorData = new AuthorData();
        $authorData->firstName  = 'Robert';
        $authorData->middleName = 'Cecil';
        $authorData->lastName   = 'Martin';

        $this->author = new Author($authorData);
    }

    public function testGetFirstName()
    {
        $this->assertEquals('Robert', $this->author->getFirstName());
    }

    public function testGetMiddleName()
    {
        $this->assertEquals('Cecil', $this->author->getMiddleName());
    }

    public function testGetLastName()
    {
        $this->assertEquals('Martin', $this->author->getLastName());
    }

    public function testGetAuthorName()
    {
        $this->assertEquals('Martin R. C.', $this->author->getAuthorName());
    }

    public function testSetFirstName()
    {
        $firstName = 'Denis';
        $this->author->setFirstName($firstName);
        $this->assertEquals($firstName, $this->author->getFirstName());
    }

    public function testSetMiddleName()
    {
        $middleName = 'MacAlister';
        $this->author->setMiddleName($middleName);
        $this->assertEquals($middleName, $this->author->getMiddleName());
    }

    public function testSetLastName()
    {
        $lastName = 'Ritchie';
        $this->author->setLastName($lastName);
        $this->assertEquals($lastName, $this->author->getLastName());
    }
}