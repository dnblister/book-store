<?php
/**
 *
 * @author  Zarubin Kirill <dnblister@gmail.com>
 *
 * @version 1.0
 */

namespace Tests\AppBundle\Entity;

use AppBundle\DTO\AuthorData;
use AppBundle\DTO\BookData;
use AppBundle\Entity\Author;
use AppBundle\Entity\Book;
use DateTime;
use PHPUnit\Framework\TestCase;
use PHPUnit\Runner\Exception;
use ReflectionClass;

class BookTest extends TestCase
{
    /**
     * @var Book
     */
    private $book;

    public function setUp()
    {
        $bookData = new BookData;
        $bookData->name = 'Clean Code: A Handbook of Agile Software Craftsmanship';
        $bookData->isbn = '978-0132350884';
        $bookData->publishDate = DateTime::createFromFormat(Book::$PUBLISH_DATE_FORMAT, '2008');

        $authorData = new AuthorData;
        $authorData->firstName  = 'Robert';
        $authorData->middleName = 'Cecil';
        $authorData->lastName   = 'Martin';

        $bookData->authors[] = new Author($authorData);
        $this->book = new Book($bookData);
    }

    public function testGetName()
    {
        $this->assertEquals('Clean Code: A Handbook of Agile Software Craftsmanship', $this->book->getName());
    }

    public function testGetISBN()
    {
        $this->assertEquals('978-0132350884', $this->book->getISBN());
    }

    public function testGetPublishDate()
    {
        $this->assertEquals('2008', $this->book->getPublishDate());
    }

    public function testGetAuthors()
    {
        $authorData = new AuthorData();
        $authorData->firstName  = 'Robert';
        $authorData->middleName = 'Cecil';
        $authorData->lastName   = 'Martin';

        $author = new Author($authorData);
        $authors = [
            $this->invokeMethod($this->book, 'getSignAuthor', [$author]) => $author
        ];

        $this->assertEquals($authors, $this->book->getAuthors());
    }

    public function invokeMethod(&$object, $methodName, array $parameters = [])
    {
        try {
            $reflection = new ReflectionClass(get_class($object));
            $method = $reflection->getMethod($methodName);
            $method->setAccessible(true);

            return $method->invokeArgs($object, $parameters);
        } catch (\ReflectionException $e) {
            throw new Exception($e->getMessage(), $e->getCode(), $e);
        }
    }

    public function testSetName()
    {
        $name = 'Clean Architecture: A Craftsman\'s Guide to Software Structure and Design';
        $this->book->setName($name);
        $this->assertEquals($name, $this->book->getName());
    }
}