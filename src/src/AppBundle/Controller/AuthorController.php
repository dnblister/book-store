<?php
/**
 *
 * @author  Zarubin Kirill <dnblister@gmail.com>
 *
 * @version 1.0
 */

namespace AppBundle\Controller;

use AppBundle\DTO\AuthorData;
use AppBundle\Entity\Author;
use AppBundle\Form\AuthorType;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AuthorController extends Controller
{
    /**
     * @Route("/author/list", name="list-authors")
     * @return Response
     */
    public function indexAction()
    {
        $authors = $this->getDoctrine()->getRepository(Author::class)->findAll();

        return $this->render('author/list.html.twig', [
            'authors' => $authors
        ]);
    }

    /**
     * @Route("/author/add", name="add-author")
     * @param Request $request
     *
     * @return Response
     */
    public function addAuthorAction(Request $request)
    {
        $form = $this->createForm(AuthorType::class, new AuthorData);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $author = new Author($form->getData());
            $em = $this->getDoctrine()->getManager();
            $em->persist($author);

            try {
                $em->flush();
            } catch (UniqueConstraintViolationException $e) {
                $this->addFlash('error', 'Author ' . $author->getAuthorName() . 'already exist.');
            }

            return $this->redirectToRoute('list-authors');
        }

        return $this->render('author/author.html.twig', [
            'title' => 'Add author',
            'form'  => $form->createView()
        ]);
    }

    /**
     * @Route("/author/edit/{id}", name="edit-author")
     * @param string  $id
     * @param Request $request
     * @return Response
     */
    public function editAuthorAction($id, Request $request)
    {
        /** @var Author $author */
        $author = $this->getDoctrine()->getRepository(Author::class)->find($id);
        $authorData = AuthorData::fromAuthor($author);

        $form = $this->createForm(AuthorType::class, $authorData);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            /** @var AuthorData $authorData */
            $authorData = $form->getData();
            $author->setFirstName($authorData->firstName);
            $author->setMiddleName($authorData->middleName);
            $author->setLastName($authorData->lastName);

            $em = $this->getDoctrine()->getManager();
            $em->persist($author);
            $em->flush();

            return $this->redirectToRoute('list-authors');
        }

        return $this->render('author/author.html.twig', [
            'title' => 'Edit author',
            'form'  => $form->createView()
        ]);
    }

    /**
     * @Route("/author/delete/{id}", name="delete-author")
     * @param string  $id
     * @param Request $request
     * @return Response
     */
    public function deleteAuthorAction($id, Request $request)
    {
        /** @var Author $author */
        $author = $this->getDoctrine()->getRepository(Author::class)->find($id);

        $form = $this->createFormBuilder()
            ->add('submit',SubmitType::class)
            ->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($author);
            $em->flush();

            return $this->redirectToRoute('list-authors');
        }

        return $this->render('author/confirm.html.twig', [
            'name' => $author->getAuthorName(),
            'form' => $form->createView()
        ]);
    }

}