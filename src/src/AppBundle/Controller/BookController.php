<?php
/**
 *
 * @author  Zarubin Kirill <dnblister@gmail.com>
 *
 * @version 1.0
 */

namespace AppBundle\Controller;

use AppBundle\DTO\BookData;
use AppBundle\Entity\Book;
use AppBundle\Form\BookType;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class BookController extends Controller
{
    /**
     * @Route("/book/list", name="list-book")
     *
     * @return Response
     */
    public function indexAction()
    {
        $books = $this->getDoctrine()->getRepository(Book::class)->findAll();

        return $this->render('book/list.html.twig', [
            'books' => $books
        ]);
    }

    /**
     * @Route("/book/add", name="add-book")
     * @param Request $request
     *
     * @return RedirectResponse|Response
     */
    public function addBookAction(Request $request)
    {
        $form = $this->createForm(BookType::class, new BookData);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $book = new Book($form->getData());
            /** @var File $image */
            $image = $book->getImage();
            $fileName = $this->saveImage($image);
            $book->setImage($fileName);

            $em = $this->getDoctrine()->getManager();
            $em->persist($book);

            try {
                $em->flush();
            } catch (UniqueConstraintViolationException $e) {
                $this->addFlash('error', 'Book ' . $book->getName() . 'already exist.');
            }

            return $this->redirectToRoute('list-book');
        }

        return $this->render('book/book.html.twig', [
            'title' => 'Add book',
            'form'  => $form->createView(),
        ]);
    }

    /**
     * @Route("/book/edit/{id}", name="edit-book")
     * @param         $id
     * @param Request $request
     *
     * @return RedirectResponse|Response
     */
    public function editBookAction($id, Request $request)
    {
        /** @var Book $book */
        $book = $this->getDoctrine()->getRepository(Book::class)->find($id);
        $bookData = BookData::fromBook($book);

        $image = new File($this->getParameter('image_directory') . '/' . $bookData->image);
        $bookData->image = $image;

        $form = $this->createForm(BookType::class, $bookData);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            /** @var BookData $bookData */
            $bookData = $form->getData();
            $book->setName($bookData->name);
            $book->setISBN($bookData->isbn);
            $book->setPublishDate($bookData->publishDate);
            $book->setPageNum($bookData->pageNum);
            $book->setAuthors($bookData->authors->toArray());

            /** @var File $image */
            $image = $bookData->image;
            $fileName = $this->saveImage($image);

            $book->setImage($fileName);

            $em = $this->getDoctrine()->getManager();
            $em->persist($book);
            $em->flush();

            return $this->redirectToRoute('list-book');
        }

        return $this->render('book/book.html.twig', [
            'title' => 'Edit book',
            'form'  => $form->createView(),
        ]);
    }

    private function saveImage(File $image)
    {
        $fileName = md5(uniqid()) . '.' . $image->guessExtension();

        try {
            $image->move($this->getParameter('image_directory'), $fileName);
        } catch (FileException $e) {
            $this->addFlash('error', 'Can\'t save image: ' . $e->getMessage());
            return $this->redirectToRoute('list-book');
        }

        return $fileName;
    }

    /**
     * @Route("/book/delete/{id}", name="delete-book")
     * @param         $id
     * @param Request $request
     *
     * @return RedirectResponse|Response
     */
    public function deleteBookAction($id, Request $request)
    {
        /** @var Book $book */
        $book = $this->getDoctrine()->getRepository(Book::class)->find($id);

        $form = $this->createFormBuilder()
            ->add('submit', SubmitType::class)
            ->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($book);
            $em->flush();

            return $this->redirectToRoute('list-book');
        }

        return $this->render('book/confirm.html.twig', [
            'name' => $book->getName(),
            'form' => $form->createView()
        ]);
    }
}