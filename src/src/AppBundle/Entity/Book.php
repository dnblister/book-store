<?php
/**
 *
 * @author  Zarubin Kirill <dnblister@gmail.com>
 *
 * @version 1.0
 */

namespace AppBundle\Entity;

use AppBundle\DTO\BookData;
use DateTime;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Tests\AppBundle\Exceptions\Entity\AuthorExistException;

/**
 * @ORM\Entity
 * @ORM\Table(name="book")
 */
class Book
{
    public static $PUBLISH_DATE_FORMAT = 'Y';

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     */
    private $id;

    /**
     * @ORM\Column(length=200)
     * @var string
     */
    private $name;

    /**
     * @ORM\Column(length=50)
     * @var string
     */
    private $isbn;

    /**
     * @ORM\Column(type="date")
     * @var DateTime
     */
    private $publishDate;

    /**
     * @ORM\Column(type="smallint")
     * @var integer
     */
    private $pageNum;

    /**
     * @ORM\ManyToMany(targetEntity="Author", inversedBy="books")
     * @ORM\JoinTable(name="book_author", joinColumns={
     *     @ORM\JoinColumn(name="book_id", referencedColumnName="id")
     * }, inverseJoinColumns={
     *     @ORM\JoinColumn(name="author_id", referencedColumnName="id")
     * })
     */
    private $authors;

    /**
     * @ORM\Column(length=100)
     * @var string
     */
    private $image;

    /**
     * @param BookData $data
     */
    public function __construct(BookData $data)
    {
        $this->name = $data->name;
        $this->isbn = $data->isbn;
        $this->publishDate = $data->publishDate;
        $this->pageNum = $data->pageNum;
        $this->authors = $data->authors->toArray();
        $this->image = $data->image;
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $isbn
     */
    public function setISBN($isbn)
    {
        $this->isbn = $isbn;
    }

    /**
     * @return string
     */
    public function getISBN()
    {
        return $this->isbn;
    }

    /**
     * @param DateTime $publishDate
     */
    public function setPublishDate(DateTime $publishDate)
    {
        $this->publishDate = $publishDate;
    }

    /**
     * @return string
     */
    public function getPublishDate()
    {
        return $this->publishDate;
    }

    /**
     * @param $pageNum
     */
    public function setPageNum($pageNum)
    {
        $this->pageNum = $pageNum;
    }

    /**
     * @return int
     */
    public function getPageNum()
    {
        return $this->pageNum;
    }

    /**
     * @param Collection[] $authors
     */
    public function setAuthors($authors)
    {
        $this->authors = $authors;
    }

    /**
     * @return array
     */
    public function getAuthors()
    {
        return $this->authors;
    }

    /**
     * @param string $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }
}