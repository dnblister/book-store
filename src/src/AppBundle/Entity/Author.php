<?php
/**
 *
 * @author  Zarubin Kirill <dnblister@gmail.com>
 *
 * @version 1.0
 */

namespace AppBundle\Entity;

use AppBundle\DTO\AuthorData;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="author")
 */
class Author
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(length=100, name="first_name")
     */
    private $firstName;

    /**
     * @var string
     * @ORM\Column(length=100, name="middle_name")
     */
    private $middleName;

    /**
     * @var string
     * @ORM\Column(length=100, name="last_name")
     */
    private $lastName;

    /**
     * @ORM\ManyToMany(targetEntity="Book", mappedBy="authors")
     * @ORM\JoinTable(name="book_author", joinColumns={
     *     @ORM\JoinColumn(name="author_id", referencedColumnName="id")
     * }, inverseJoinColumns={
     *     @ORM\JoinColumn(name="book_id", referencedColumnName="id")
     * })
     */
    private $books;

    /**
     * @param AuthorData $data
     */
    public function __construct(AuthorData $data)
    {
        $this->firstName  = $data->firstName;
        $this->middleName = $data->middleName;
        $this->lastName   = $data->lastName;
    }

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $firstName
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param string $middleName
     */
    public function setMiddleName($middleName)
    {
        $this->middleName = $middleName;
    }

    /**
     * @return string
     */
    public function getMiddleName()
    {
        return $this->middleName;
    }

    /**
     * @param string $lastName
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @return string
     */
    public function getAuthorName()
    {
        return $this->lastName . ' ' . $this->firstName[0] . '. ' . $this->middleName[0] . '.';
    }

    /**
     * @return Collection[]
     */
    public function getBooks()
    {
        return $this->books;
    }
}