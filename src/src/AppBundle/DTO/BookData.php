<?php
/**
 *
 * @author  Zarubin Kirill <dnblister@gmail.com>
 *
 * @version 1.0
 */

namespace AppBundle\DTO;

use AppBundle\Entity\Book;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Validator\Constraints as Assert;

class BookData
{
    /**
     * @Assert\NotBlank()
     */
    public $name;

    /**
     * @Assert\NotBlank()
     * @Assert\Isbn(type="isbn13")
     */
    public $isbn;

    /**
     * @Assert\Date()
     */
    public $publishDate;

    /**
     * @Assert\NotBlank()
     */
    public $pageNum;

    /**
     * @Assert\NotBlank()
     * @var Collection
     */
    public $authors;

    /**
     * @Assert\Image(mimeTypes={"image/jpeg", "image/png"})
     */
    public $image;

    public static function fromBook(Book $book)
    {
        $bookData = new self();
        $bookData->name = $book->getName();
        $bookData->isbn = $book->getISBN();
        $bookData->publishDate = $book->getPublishDate();
        $bookData->pageNum = $book->getPageNum();
        $bookData->authors = $book->getAuthors();
        $bookData->image = $book->getImage();

        return $bookData;
    }
}