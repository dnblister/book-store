<?php
/**
 *
 * @author  Zarubin Kirill <dnblister@gmail.com>
 *
 * @version 1.0
 */

namespace AppBundle\DTO;


use AppBundle\Entity\Author;
use Symfony\Component\Validator\Constraints as Assert;

class AuthorData
{
    /**
     * @Assert\NotBlank()
     * @Assert\Length(min="2", max="100")
     * @var string
     */
    public $firstName;

    /**
     * @Assert\NotBlank()
     * @Assert\Length(min="2", max="100")
     * @var string
     */
    public $middleName;

    /**
     * @Assert\NotBlank()
     * @Assert\Length(min="2", max="100")
     * @var string
     */
    public $lastName;

    /**
     * @param Author $author
     * @return AuthorData
     */
    public static function fromAuthor(Author $author)
    {
        $authorData = new self();
        $authorData->firstName  = $author->getFirstName();
        $authorData->middleName = $author->getMiddleName();
        $authorData->lastName   = $author->getLastName();

        return $authorData;
    }
}