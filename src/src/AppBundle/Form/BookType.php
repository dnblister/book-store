<?php
/**
 *
 * @author  Zarubin Kirill <dnblister@gmail.com>
 *
 * @version 1.0
 */

namespace AppBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class BookType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class)
            ->add('isbn', TextType::class)
            ->add('publishDate', DateType::class, [
                'widget' => 'single_text',
                'format' => 'y'
            ])->add('pageNum', TextType::class)
            ->add('authors', EntityType::class, [
                'class'        => 'AppBundle:Author',
                'choice_label' => 'authorName',
                'multiple'     => true
            ])->add('image', FileType::class)
            ->add('save', SubmitType::class);
    }
}