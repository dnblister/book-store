DROP TABLE IF EXISTS author;
CREATE TABLE author (
  id          SERIAL NOT NULL,
  first_name  VARCHAR(100),
  middle_name VARCHAR(100),
  last_name   VARCHAR(100),
  CONSTRAINT unique_author_name_key
  UNIQUE (first_name, middle_name, last_name)
);

DROP TABLE IF EXISTS book;
CREATE TABLE book (
  id           SERIAL NOT NULL,
  name         VARCHAR(200),
  isbn         VARCHAR(50),
  publish_date DATE,
  page_num     SMALLINT,
  image        VARCHAR(100),
  CONSTRAINT unique_book_key
  UNIQUE (name, isbn)
);

DROP TABLE IF EXISTS book_author;
CREATE TABLE book_author (
  book_id   INTEGER,
  author_id INTEGER,
  PRIMARY KEY (book_id, author_id)
);